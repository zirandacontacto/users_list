package com.example.users_list

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import com.example.users_list.Model.Clientes
import com.example.users_list.databinding.ActivityAgregarEmpleadosBinding
import java.util.UUID

class Agregar_Clientes : AppCompatActivity() {
    private lateinit var binding: ActivityAgregarEmpleadosBinding
    private val realmApplication by lazy { RealmApplication() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAgregarEmpleadosBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.button.setOnClickListener {
            insertar()
        }
    }

    private fun insertar(){
        val empleado = Clientes(
            UUID.randomUUID().toString().substring(0,10),binding.etNombre.text.toString()
            ,binding.etApellido.text.toString()
            ,binding.etDireccion.text.toString())
        if (realmApplication.checkIfExist(empleado)){
            Handler().post(){
                Toast.makeText(this,"El usuario ya existe", Toast.LENGTH_LONG).show()
            }
        }else{
            //agregar empleado a firebase
            Manejador_Firebase().insertarDato(empleado)
            //agregar empleado local
            realmApplication.addClientOrUpdate(empleado)
        }
        finish()
    }

}