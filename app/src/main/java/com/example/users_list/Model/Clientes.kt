package com.example.users_list.Model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
open class Clientes(
    @PrimaryKey
    var id:String? = null,
    var nombre: String = "",
    var apellido: String = "",
    var direccion: String = ""
): RealmObject(),Parcelable