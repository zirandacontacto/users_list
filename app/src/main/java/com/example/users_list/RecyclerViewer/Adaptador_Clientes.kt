package com.example.users_list.RecyclerViewer

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.users_list.Model.Clientes
import com.example.users_list.databinding.ItemListaEmpleadosBinding

class Adaptador_Clientes (private val emplist: ArrayList<Clientes>,
                          val itemClickListener:(Clientes)->Unit
) : RecyclerView.Adapter<Adaptador_Clientes.ViewHolder>() {

    inner class ViewHolder(val binding: ItemListaEmpleadosBinding) : RecyclerView.ViewHolder(binding.root){
        fun bind(get: Clientes) = with(binding){
            tvNombre.text = get.nombre
            tvApellido.text = get.apellido
            tvDireccion.text = get.direccion
            root.setOnClickListener {
                itemClickListener(get)
            }
        }
    }

    //vista
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemListaEmpleadosBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(emplist.get(position))
    }

    //posicion
    override fun getItemCount(): Int {
        return emplist.size
    }
}