package com.example.users_list

import com.example.users_list.Model.Clientes
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.RealmResults

class RealmApplication{
        val config: RealmConfiguration =
            RealmConfiguration.Builder()
                .name("Empleados")
                .deleteRealmIfMigrationNeeded()
                .schemaVersion(1).build()

        val realm: Realm = Realm.getInstance(config)

        //use it to save or update the data
        fun addClientOrUpdate(cliente: Clientes) {
            realm.beginTransaction()
            cliente.let { clientes ->
                realm.insertOrUpdate(clientes)
                realm.commitTransaction()
            }
        }

        //read all data
        val getAllClient: RealmResults<Clientes> by lazy {
            realm.where(Clientes::class.java).findAll()
        }

        fun checkIfExist(clientes: Clientes): Boolean {
            val BD = getAllClient
            return BD.any { it.nombre == clientes.nombre && it.apellido == clientes.apellido }
        }

        fun oneItem(id: String): Clientes? {
            return realm.where(Clientes::class.java).equalTo("id", id).findFirst()
        }

        fun deleteFromRealm(id: String) {
            val itemRealmObject = realm.where(Clientes::class.java).equalTo("id", id).findFirst()
            if (itemRealmObject != null) {
                realm.beginTransaction()
                itemRealmObject.deleteFromRealm()
                realm.commitTransaction()
            }
        }


}