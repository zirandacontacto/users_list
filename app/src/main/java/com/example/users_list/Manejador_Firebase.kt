package com.example.users_list

import com.example.users_list.Model.Clientes
import com.google.firebase.firestore.FirebaseFirestore

class Manejador_Firebase(){
    val db = FirebaseFirestore.getInstance()
    val dataInsertar = HashMap<String, Any>()

    fun insertarDato(clientes: Clientes){
        dataInsertar["Nombre"] = clientes.nombre
        dataInsertar["Apellido"] = clientes.apellido
        dataInsertar["Direccion"] = listOf(clientes.direccion)

        db.collection("Empleados").add(dataInsertar).addOnSuccessListener { documentReference ->
            println("Registro exitoso")
        }
            .addOnFailureListener{
                    e -> println(e.message.toString())
            }
    }

    fun updateDatos(clientes: Clientes){
        dataInsertar["Nombre"] = clientes.nombre
        dataInsertar["Apellido"] = clientes.apellido
        dataInsertar["Direccion"] = listOf(clientes.direccion)
        db.collection("Empleados").document(clientes.id.toString()).update(dataInsertar).addOnSuccessListener {
            println("Update exitoso")
        }
            .addOnFailureListener{
                    e -> println(e.message.toString())
            }
    }

    fun deleteDatos(clientes: Clientes){
        db.collection("Empleados").document(clientes.id.toString()).delete().addOnSuccessListener {
            println("Delete Exitosos")
        } .addOnFailureListener {
            e-> println(e.message.toString())
        }
    }
}

