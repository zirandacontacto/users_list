package com.example.users_list

import android.app.Application
import io.realm.Realm

class MyAppRealm: Application() {
    override fun onCreate() {
        super.onCreate()
        Realm.init(this)
    }
}