package com.example.users_list

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.users_list.Model.Clientes
import com.example.users_list.databinding.FragmentActualizarEmpleadosBinding
import com.google.firebase.firestore.FirebaseFirestore

class Actualizar_Clientes(idValue: String?) : Fragment() {
    private lateinit var binding: FragmentActualizarEmpleadosBinding
    private val db= FirebaseFirestore.getInstance()
    private val coleccion = db.collection("Empleados")
    private val idSearch = idValue
    private val realmApplication by lazy { RealmApplication() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentActualizarEmpleadosBinding.inflate(inflater, container, false)

        if (!isConnectedToInternet(requireContext())){
            // manda la informacion desconectado
            val document = realmApplication.oneItem(idSearch.toString())
            binding.etNombre.setText(document?.nombre)
            binding.etApellido.setText(document?.apellido)
            binding.etDireccion.setText(document?.direccion)
            binding.button.setOnClickListener { actualizar(document?.id) }
            binding.buttonDelete.setOnClickListener {
                eliminar(document?.id.toString()) }
        }else{
            //manda la informacion conectado
            coleccion.document(idSearch.toString()).get().addOnSuccessListener { Empleados ->
                binding.etNombre.setText(Empleados.getString("Nombre"))
                binding.etApellido.setText(Empleados.getString("Apellido"))
                val direccion = Empleados.get("Direccion") as List<String>
                binding.etDireccion.setText(direccion[0])
                binding.button.setOnClickListener { actualizar(Empleados.id) }
                binding.buttonDelete.setOnClickListener {
                    eliminar(Empleados.id)
                }
            }
                .addOnFailureListener{exception ->
                    println(exception)
                }
        }
        return binding.root
    }

    fun actualizar(id: String?){
        val empleado = Clientes(id,binding.etNombre.text.toString()
            ,binding.etApellido.text.toString()
            ,binding.etDireccion.text.toString())

        if (!isConnectedToInternet(requireContext())){
            //actualizar realm
            realmApplication.addClientOrUpdate(empleado)
        }else{
            // actualizar firebase
            Manejador_Firebase().updateDatos(empleado)
        }
        parentFragmentManager.popBackStack()
    }

    fun eliminar(id: String?){
        val empleado = Clientes(id,binding.etNombre.text.toString()
            ,binding.etApellido.text.toString()
            ,binding.etDireccion.text.toString())

        realmApplication.deleteFromRealm(id.toString())
        Manejador_Firebase().deleteDatos(empleado)
        parentFragmentManager.popBackStack()
    }

    private fun getConnectivityManager(context: Context): ConnectivityManager {
        return context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    }

    private fun isConnectedToInternet(context: Context): Boolean {
        val connectivityManager = getConnectivityManager(context)
        val activeNetwork = connectivityManager.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnected
    }

}