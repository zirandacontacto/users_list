package com.example.users_list

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.users_list.Model.Clientes
import com.example.users_list.RecyclerViewer.Adaptador_Clientes
import com.example.users_list.databinding.FragmentEmpleadosListaBinding
import com.google.firebase.firestore.FirebaseFirestore

class Clientes_Lista_Fragment : Fragment() {
    private lateinit var binding: FragmentEmpleadosListaBinding
    private var emptyList:ArrayList<Clientes> = ArrayList<Clientes>()
    private val db= FirebaseFirestore.getInstance()
    private val coleccion = db.collection("Empleados")
    private val realmApplication by lazy { RealmApplication() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentEmpleadosListaBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val clientes=realmApplication.getAllClient
        if (!isConnectedToInternet(requireContext())){
            emptyList.clear()
            if (clientes.size == 0){
                binding.textView2.visibility = View.VISIBLE
            } else {
                binding.textView2.visibility = View.INVISIBLE
            for (cliente in clientes){
                    val ID = cliente.id
                    val nombre = cliente.nombre
                    val apellido = cliente.apellido
                    val direccion = cliente.direccion
                    emptyList.add(Clientes(ID, nombre, apellido, direccion))
                }
                val itemAdapter = Adaptador_Clientes(emptyList) { cliente1 ->
                    val fragmentManager = requireActivity().supportFragmentManager
                    val fragmentTransaction = fragmentManager.beginTransaction()
                    fragmentTransaction.replace(
                        ((view as ViewGroup).parent as View).id,
                        Actualizar_Clientes(cliente1.id),
                    )
                    fragmentTransaction.addToBackStack(null)
                    fragmentTransaction.commit()
                }
                val recyclerView: RecyclerView = view.findViewById(R.id.recyclerView)
                recyclerView.layoutManager = LinearLayoutManager(context)
                recyclerView.adapter = itemAdapter
            }
        }else {
            coleccion.get().addOnSuccessListener { querySnapshot ->
                for (document in querySnapshot) {
                    val ID = document.id
                    val nombre = document.getString("Nombre")
                    val apellido = document.getString("Apellido")
                    val direccion = document.get("Direccion") as List<String>
                    val direccion1 = direccion[0]
                    emptyList.add(Clientes(ID, nombre.toString(), apellido.toString(), direccion1))
                }
                if(emptyList.isNotEmpty()) binding.textView2.visibility = View.INVISIBLE
                val itemAdapter = Adaptador_Clientes(emptyList) { empleados ->
                    val fragmentManager = requireActivity().supportFragmentManager
                    val fragmentTransaction = fragmentManager.beginTransaction()
                    fragmentTransaction.replace(
                        ((view as ViewGroup).parent as View).id,
                        Actualizar_Clientes(empleados.id),
                    )
                    fragmentTransaction.addToBackStack(null)
                    fragmentTransaction.commit()
                }
                val recyclerView: RecyclerView = view.findViewById(R.id.recyclerView)
                recyclerView.layoutManager = LinearLayoutManager(context)
                recyclerView.adapter = itemAdapter
            }
            emptyList.clear()
        }
    }

    private fun getConnectivityManager(context: Context): ConnectivityManager {
        return context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    }

    private fun isConnectedToInternet(context: Context): Boolean {
        val connectivityManager = getConnectivityManager(context)
        val activeNetwork = connectivityManager.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnected
    }


}